import React, { Component } from "react";

class Task extends Component {
  state = {};
  render() {
    return (
      <>
        <span style={{ width: "70%" }}>
          {this.props.task.text} | dodane: {this.props.task.date} |{" "}
          {this.props.task.priority === "priority" ? (
            <span className="priority">Pilne</span>
          ) : (
            <span className="normal">zwykłe</span>
          )}
        </span>
        <span style={{ width: "a" }}>
          <button onClick={() => this.props.removeTask(this.props.task.id)}>
            usuń
          </button>
          <button
            className="done"
            onClick={() => this.props.done(this.props.task.id)}
          >
            gotowe
          </button>
        </span>
      </>
    );
  }
}

export default Task;
