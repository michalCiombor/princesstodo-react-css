import React, { Component } from "react";
import "./App.css";
import Task from "./Task";

class App extends Component {
  //input change plus setState change
  counter = 0;

  state = { value: "", tasks: [], priority: false, tasksDone: [] };

  handleChange = e => {
    this.setState({
      value: e.target.value
    });
  };

  //adding task to tasksList
  addTask = () => {
    if (this.state.value && this.state.value !== " ") {
      const task = {
        id: this.counter,
        text: this.state.value,
        date: new Date().toLocaleString(),
        priority: this.state.priority ? "priority" : "normal",
        active: true,
        finish: ""
      };
      this.setState({
        tasks: this.state.tasks.concat(task),
        value: "",
        priority: false
      });
      this.counter++;
      console.log(this.state.tasks);
    } else alert("please add a task first :)");
  };
  //removing task from list
  removeTask = id => {
    let tasks = [...this.state.tasks];
    const index = tasks.findIndex(task => task.id === id);
    tasks.splice(index, 1);
    this.setState({
      tasks
    });
  };
  //checkbox change for priority
  handleCheck = () => {
    this.setState({
      priority: !this.state.priority
    });
  };
  //moving task from main list to done list
  taskDone = id => {
    let finishDate = new Date().toLocaleString();
    console.log(id);
    let tasksDone = [...this.state.tasksDone];
    let tasks = [...this.state.tasks];
    tasks = tasks.filter(task => task.id === id);
    // eslint-disable-next-line
    tasks.map(task => {
      task.active = false;
      task.finish = finishDate;
    });
    let taskDone = tasks.filter(task => !task.active);
    console.log(taskDone);
    tasksDone = tasksDone.concat(taskDone);

    this.setState({
      tasksDone
    });
  };
  clearDone = () => {
    this.setState({
      tasksDone: []
    });
  };
  //APP RENDER
  render() {
    let tasks2 = [...this.state.tasks];
    tasks2 = tasks2.filter(task => task.active === true);
    // let prior = this.state.priority;

    tasks2 = tasks2.map(task => (
      <li key={task.id}>
        <Task
          removeTask={this.removeTask}
          task={task}
          done={this.taskDone}
          // prior={this.state.priority ? true : false}
        />
      </li>
    ));
    return (
      <div className="app">
        <div className="header">
          <h1>-&nbsp;Księżniczkowa lista zadań&nbsp;-</h1>
        </div>
        <div className="control">
          <input
            placeholder="wpisz zadanie..."
            type="text"
            value={this.state.value}
            onChange={this.handleChange}
          />

          <input
            id="checkbox"
            type="checkbox"
            checked={this.state.priority}
            onChange={this.handleCheck}
          />
          <label htmlFor="checkbox">Pilne</label>
          <button onClick={this.addTask}>- Dodaj -</button>
        </div>

        <div className="taskList">
          <ul>
            {tasks2.length > 0 ? tasks2 : <h2>-&nbsp;BRAK ZADAŃ DO WYKONANIA&nbsp;-</h2>}
          </ul>
          <hr />
          <ul style={{ listStyleType: "none" }}>
            {this.state.tasksDone.length > 0 ? (
              this.state.tasksDone.map(task => (
                <li key={task.id}>
                  <span
                    style={{
                      fontSize: "19px",
                      color: "gray",
                      fontStyle: "italic",
                      letterSpacing: "1px"
                    }}
                  >
                    Ukończono: {task.text} | {task.finish}
                  </span>
                </li>
              ))
            ) : (
              <h2>-&nbsp;BRAK ZADAŃ WYKONANYCH&nbsp;-</h2>
            )}
          </ul>
          {this.state.tasksDone.length > 0 ? (
            <button onClick={this.clearDone}>- WYCZYŚĆ -</button>
          ) : null}
        </div>
      </div>
    );
  }
}

export default App;
